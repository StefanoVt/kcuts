#!/usr/bin/env python2
from setuptools import find_packages, setup

setup(
    name='kcuts',
    version='1.0.1',
    packages=find_packages(exclude=['tests']),
    install_requires=['networkx'],
    test_suite='tests', )