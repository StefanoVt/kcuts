from __future__ import print_function
from __future__ import print_function
from __future__ import print_function
from __future__ import print_function
from __future__ import print_function
from unittest import TestCase

import networkx as nx
from os.path import dirname

import kcuts.kcuts as kcuts
from kcuts.kcuts import is_correct


class TestAll_cuts(TestCase):
    def test_correct(self):
        self._test_all_cuts("c17")
        self._test_all_cuts("c499")
        self._test_all_cuts("c1908")

    def _test_all_cuts(self, name):
        with open(dirname(__file__) + "/" + name + ".adjlist", "rb") as f:
            g = nx.read_edgelist(f, create_using=nx.DiGraph())
        for top in map(str, filter(lambda n: g.out_degree(n) == 0, g.nodes())):
            all_cuts = kcuts.all_cuts(g, top, 5)
            for cut in all_cuts:
                self.assertTrue(is_correct(g, top, cut))
            all_cuts2 = kcuts.all_cuts2(g, top, 5)
            for cut in all_cuts2:
                self.assertTrue(is_correct(g, top, cut))
            #all_cuts3 = kcuts.all_cuts3(g, top, 5)
            #for cut in all_cuts3:
            #   self.assertTrue(is_correct(g, top, cut))
            self.assertEqual(set(all_cuts), set(all_cuts2))
            #self.assertEqual(set(all_cuts), set(all_cuts3))

    def test_speed(self):
        with open(dirname(__file__) + "/c499.adjlist", "rb") as f:
            g = nx.read_edgelist(f, create_using=nx.DiGraph())
        tops = map(str, range(g.number_of_nodes()))

        #print(kcuts.all_global_cuts(g, u'400', 4))
        #print(kcuts.all_global_cuts3(g, u'400', 4))

        print([kcuts.all_cuts(g, top, 5) for top in tops])

    def test_speed2(self):
        with open(dirname(__file__) + "/c499.adjlist", "rb") as f:
            g = nx.read_edgelist(f, create_using=nx.DiGraph())
        tops = map(str, range(g.number_of_nodes()))

        print([kcuts.all_cuts2(g, top, 5) for top in tops])

    def test_speed3(self):
        with open(dirname(__file__) + "/c499.adjlist", "rb") as f:
            g = nx.read_edgelist(f, create_using=nx.DiGraph())
        tops = map(str, range(g.number_of_nodes()))

        print([kcuts.all_cuts3(g, top, 5) for top in tops])
