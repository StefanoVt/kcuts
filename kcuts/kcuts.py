import networkx as nx
from itertools import product
from typing import Optional, Hashable, FrozenSet, List, Set


def combine_cuts(allcuts, k):
    """Given the possible cuts for the children, enumerate the cuts for the node"""
    for subcut in product(*allcuts):
        sc = set()
        sclen = 0
        for ss in subcut:
            sc.update(ss)
            sclen = len(sc)
            if sclen > k:
                break #next cut already, do not finish
        if 0 < sclen <= k:
            # immutable set can be inserted in sets/dicts
            yield frozenset(sc)


def combine_cuts2(allcuts,k):
    if len(allcuts) == 1 :
        for ss in allcuts[0]:
            ret = frozenset(ss)
            #assert len(ret) <= k
            yield ret
    elif len(allcuts) > 1:
        head, tail = allcuts[0], allcuts[1:]
        for subcut in combine_cuts2(tail, k):
            for ss in head:
                ret = subcut | ss
                if len(ret) <= k:
                    yield ret




def all_cuts(graph, node, k, memo=None):
    # type: (nx.DiGraph, Hashable, int, Optional[dict]) -> Set[FrozenSet[Hashable]]
    """Calculate all k-cuts of a vertex

    mode is the direction of the arrows in the DAG, by default children point to the parent"""

    if memo is None:
        memo = dict()
    elif node in memo:
        return memo[node]


    pred = [x[0] for x in graph.in_edges(node)]

    # start with the trivial cut
    ret = {frozenset({node})}
    childcuts = list(map(lambda x: all_cuts(graph, x, k, memo), pred))
    ret.update(validate_cut(graph, node, c) for c in  combine_cuts2(childcuts, k))

    memo[node] = ret
    return ret


def all_cuts_iter(graph, node, k):
    yield frozenset({node})

    childcuts = list(map(lambda x: all_cuts_iter(graph, x, k), graph.in_edges(node)))

    for cut in combine_cuts(childcuts, k):
        yield cut


def all_local_cuts(graph, node, k, memo=None):
    if memo is None:
        memo = dict()
    elif node in memo:
        return memo[node]

    def dual(noded):
        if graph.out_degree(noded) > 1:
            return []
        else:
            return all_local_cuts(graph, noded, k, memo)

    ret = [frozenset({node})]
    childcuts = [dual(x[0]) for x in graph.in_edges(node)]
    ret.extend(combine_cuts2(childcuts, k))
    memo[node] = ret
    return ret

def all_global_cuts(graph, node, k, memo=None, memol=None):
    if memo is None:
        memo = dict()
        memol = dict()
    elif node in memo:
        return memo[node]

    ret = [frozenset({node})]
    childcuts = [all_global_cuts(graph, x[0], k, memo, memol) for x in graph.in_edges(node)]
    my_local_cuts = (all_local_cuts(graph, node,k, memol))
    ret.extend(c for c in combine_cuts2(childcuts, k) if c not in my_local_cuts)
    memo[node] = ret
    return ret

def all_cuts2(graph, node, k, memog=None, memol=None):
    if memog is None:
        memog = dict()
        memol = dict()

    gcuts = all_global_cuts(graph, node, k, memog, memol)
    ret = set()

    for gcut in gcuts:
        expanded_cuts = [all_local_cuts(graph, subnode, k, memol) for subnode in gcut]
        ret.update(validate_cut(graph, node, c) for c in  combine_cuts(expanded_cuts, k))
        #ret.extend(validate_cut(graph, node, c) for c in  combine_cuts(expanded_cuts, k))


    return ret

def all_local_cuts3(graph, node, k, memo=None):
    if memo is None:
        memo = dict()
    elif node in memo:
        return memo[node]

    def dual(noded):
        if graph.out_degree(noded) > 1:
            return []
        else:
            return all_local_cuts3(graph, noded, k, memo)

    ret = [frozenset({node})]
    childcuts = [dual(x[0]) for x in graph.in_edges(node)]
    ret.extend(combine_cuts(childcuts, k))
    memo[node] = ret
    return ret

def all_global_cuts3(graph, node, k, memo=None, memol=None):
    if memo is None:
        memo = dict()
        memol = dict()
    elif node in memo:
        return memo[node]
    ret = {frozenset({node})}
    childcuts = [all_global_cuts3(graph, x[0], k, memo, memol) for x in graph.in_edges(node)]
    ret.update(c for c in combine_cuts(childcuts, k) if c not in all_local_cuts3(graph, node,k, memol))
    ret2 = frozenset(c for c in ret if not any(x.issubset(c) and x != c for x in ret))
    assert ret2 == {validate_cut(graph, node, c) for c in ret}, ({validate_cut(graph, node, c) for c in ret}, ret2)
    memo[node] = ret
    return ret



def all_cuts3(graph, node, k, memog=None, memol=None):
    if memog is None:
        memog = dict()
        memol = dict()

    gcuts = all_global_cuts3(graph, node, k, memog, memol)
    ret = []

    for gcut in gcuts:
        expanded_cuts = [all_local_cuts3(graph, subnode, k, memol) for subnode in gcut]
        #ret.extend( combine_cuts(expanded_cuts, k))
        ret.extend(validate_cut(graph, node, c) for c in combine_cuts(expanded_cuts, k))

    return ret

def is_correct(graph, node, cut):
    assert nx.is_directed_acyclic_graph(graph)
    touched_cut = set()
    search = {node}
    if node in cut:
        touched_cut.add(node)
    search.difference_update(cut)
    cut = set(cut)
    while len(search):
        n = search.pop()
        if graph.in_degree(n) == 0:
            return False
        search.update(x[0] for x in graph.in_edges(n))
        touched_cut.update(search.intersection(cut))
        search.difference_update(cut)
    assert touched_cut == cut, (node, cut, touched_cut)
    return True

def validate_cut(graph, node, cut):
    #assert nx.is_directed_acyclic_graph(graph)
    touched_cut = set()
    search = {node}
    if node in cut:
        touched_cut.add(node)
    search.difference_update(cut)
    cut = set(cut)
    while len(search):
        n = search.pop()
        assert graph.in_degree(n) != 0
        search.update(x[0] for x in graph.in_edges(n))
        touched_cut.update(search.intersection(cut))
        search.difference_update(cut)
    return frozenset(touched_cut)